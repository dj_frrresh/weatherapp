## WeatherApp
Test application to determine the weather in the current location and selected city. There is a function to save the city from the search as the main one.

## Requirements
- iOS 14.0+
- Xcode 14.0+

## Structure 
* "Application": Application launch files (info.plist, AppDelegate.swift).
* "Main": The main and only screen of the application displaying the weather in degrees Celsius, a search bar, and a button to save the city.
* "Services": Models and file managers for working with API.
* "Resources": Assets.
  
## Dependencies
[CocoaPods](https://cocoapods.org) is used as a dependency manager.
List of dependencies: 
* pod 'EasyPeasy' -> A library that makes it easy to organize constraints and animations for the user interface.

### Installation
Place the following code in your `Podfile`:
```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '14.0'

target 'weatherApp' do
  use_frameworks!

  pod 'EasyPeasy'

end

post_install do |installer|
    installer.generated_projects.each do |project|
        project.targets.each do |target|
            target.build_configurations.each do |config|
                config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = '14.0'
            end
        end
    end
end
```

To access the contents of the pods, import modules wherever you import UIKit or Foundation:

``` swift
import UIKit
import EasyPeasy
```

## API 
The weather API is used from the site - https://api.openweathermap.org.<br>
For the API to work, you need to register on this site and create an API key. The key is stored in <b>weatherApp->Services->WeatherManager</b>.swift in the static constant <b>apiKey</b>.

## Meta
Gmail – kunavinjenya55@gmail.com<br>
Telegram - https://t.me/just_eugeny<br>
LinkedIn - https://www.linkedin.com/in/kunav1n/