//
//  WeatherData.swift
//  weatherApp
//
//  Created by Eugene on 01.06.24.
//

import Foundation


struct WeatherData: Decodable {
    let name: String
    let main: Main
    let weather: [Weather]
}

struct Main: Decodable {
    let temp: Float
}

struct Weather: Decodable {
    let id: Int
}
