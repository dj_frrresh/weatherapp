//
//  WeatherManager.swift
//  weatherApp
//
//  Created by Eugene on 01.06.24.
//

import Foundation


protocol WeatherProtocol: AnyObject {
    func updateWeather(_ weatherManager: WeatherManager, weather: WeatherModel)
    func weatherError(error: Error)
}

class WeatherManager {
    
    static let shared = WeatherManager()
    
    private static let apiKey = "2d33e39dc8379a0edad800353bc74d2e"
    private let weatherURL = "https://api.openweathermap.org/data/2.5/weather?appid=\(apiKey)&units=metric"
    
    weak var weatherDelegate: WeatherProtocol?
        
    // Search weather by city name
    func getWeather(for cityName: String) {
        let urlString = "\(weatherURL)&q=\(cityName)"
        
        performRequest(with: urlString)
    }
    // Search for weather at the current location by coordinates
    func getWeather(forLat lat: Float, forLon lon: Float) {
        let urlString = "\(weatherURL)&lat=\(lat)&lon=\(lon)"
        
        performRequest(with: urlString)
    }
    
    private func parseJSON(_ weatherData: Data) -> WeatherModel? {
        let decoder = JSONDecoder()
        
        do {
            let decodedData = try decoder.decode(WeatherData.self, from: weatherData)
            let name = decodedData.name
            let id = decodedData.weather[0].id
            let temp = decodedData.main.temp
            
            return WeatherModel(cityName: name, precipitationCode: id, temperature: temp)
        } catch {
            DispatchQueue.main.async { [weak self] in
                self?.weatherDelegate?.weatherError(error: error)
            }
            
            return nil
        }
    }
    
    private func performRequest(with urlString: String) {
        guard let url = URL(string: urlString) else { return }
        
        let urlSession = URLSession(configuration: .default)
        let task = urlSession.dataTask(with: url) { [weak self] data, response, error in
            if let error = error {
                DispatchQueue.main.async {
                    self?.weatherDelegate?.weatherError(error: error)
                }
                
                return
            }
            
            if let data = data, let weather = self?.parseJSON(data) {
                DispatchQueue.main.async {
                    self?.weatherDelegate?.updateWeather(self!, weather: weather)
                }
            }
        }
        
        task.resume()
    }
    
}

