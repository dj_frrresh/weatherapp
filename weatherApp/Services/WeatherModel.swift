//
//  WeatherModel.swift
//  weatherApp
//
//  Created by Eugene on 01.06.24.
//

import Foundation


struct WeatherModel {
    let cityName: String
    let precipitationCode: Int
    let temperature: Float
    
    var tempString: String {
        return String(format: "%.1f", temperature)
    }
    var precipitation: String{
        switch precipitationCode {
        case 200...232, 801...804:
            return "cloud.bolt"
        case 300...321:
            return "cloud.drizzle"
        case 500...531:
            return "cloud.rain"
        case 600...622:
            return "snowflake"
        case 701...781:
            return "cloud.fog"
        case 800:
            return "sun.max"
        default:
            return "sun.max"
        }
    }
}
