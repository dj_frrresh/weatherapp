//
//  MainLocationDelegate.swift
//  weatherApp
//
//  Created by Eugene on 02.06.24.
//

import CoreLocation


extension MainVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            locationManager.stopUpdatingLocation()
            
            latitude = Float(location.coordinate.latitude)
            longitude = Float(location.coordinate.longitude)
            
            weatherManager.getWeather(forLat: latitude, forLon: longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            print("AUTH ALL")
            locationManager.requestLocation()
        case .authorizedWhenInUse:
            print("AUTH when")
            locationManager.requestLocation()
        case .denied:
            print("AUTH denied")
        case .restricted:
            print("AUTH restricted")
        case .notDetermined:
            print("AUTH not determined")
        @unknown default:
            print("AUTH update")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        print("Location error: \(error)")
    }
    
}
