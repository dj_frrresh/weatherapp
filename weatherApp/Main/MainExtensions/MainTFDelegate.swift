//
//  MainTFDelegate.swift
//  weatherApp
//
//  Created by Eugene on 02.06.24.
//

import UIKit


extension MainVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchBarTF.endEditing(true)

        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let cityName = textField.text {
            weatherManager.getWeather(for: cityName)
        }
        
        textField.text = ""
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text != "" {
            textField.placeholder = "Search"
            
            return true
        } else {
            textField.placeholder = "Enter a city name"
            
            return false
        }
    }
    
}
