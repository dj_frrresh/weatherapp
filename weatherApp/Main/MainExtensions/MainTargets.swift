//
//  MainTargets.swift
//  weatherApp
//
//  Created by Eugene on 02.06.24.
//

import UIKit


extension MainVC {
    
    @objc func getCurrentLocation(_ sender: UIButton) {
        if let savedCity = UserDefaults.standard.string(forKey: "mainCity") {
            weatherManager.getWeather(for: savedCity)
            
            return
        }
        
        locationManager.requestLocation()
        weatherManager.getWeather(forLat: latitude, forLon: longitude)
    }
    
    @objc func startSearching(_ sender: UIButton) {
        searchBarTF.endEditing(true)
    }
    
    @objc func selectMainCity(_ sender: UIButton) {
        if let city = cityLabel.text, !city.isEmpty {
            UserDefaults.standard.setValue(city, forKey: "mainCity")
            showAlert(title: "Success!", message: "The city was chosen as the main one")
        }
    }
    
    private func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
    
}
