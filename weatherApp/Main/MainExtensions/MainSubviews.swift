//
//  MainSubviews.swift
//  weatherApp
//
//  Created by Eugene on 02.06.24.
//

import EasyPeasy


extension MainVC {
    
    func mainSubviews() {
        view.addSubview(currentLocationButton)
        view.addSubview(searchButton)
        view.addSubview(searchBarTF)
        view.addSubview(precipitationIV)
        view.addSubview(tempLabel)
        view.addSubview(cityLabel)
        view.addSubview(mainCityButton)

        currentLocationButton.easy.layout([
            Width(32),
            Height(32),
            Left(16),
            Top(16).to(view.safeAreaLayoutGuide, .top)
        ])
        
        searchButton.easy.layout([
            Width(32),
            Height(32),
            Right(16),
            Top(16).to(view.safeAreaLayoutGuide, .top)
        ])
        
        searchBarTF.easy.layout([
            Height(32),
            Left(8).to(currentLocationButton, .right),
            Right(8).to(searchButton, .left),
            Top(16).to(view.safeAreaLayoutGuide, .top)
        ])
        
        precipitationIV.easy.layout([
            CenterX(),
            Height(80),
            Width(80),
            Top(80).to(searchBarTF, .bottom)
        ])
        
        tempLabel.easy.layout([
            CenterX(),
            Left(32),
            Right(32),
            Top(16).to(precipitationIV, .bottom)
        ])
        
        cityLabel.easy.layout([
            CenterX(),
            Left(32),
            Right(32),
            Top(16).to(tempLabel, .bottom)
        ])
        
        mainCityButton.easy.layout([
            Left(32),
            Right(32),
            Height(52),
            Top(32).to(cityLabel, .bottom)
        ])
    }
    
}
