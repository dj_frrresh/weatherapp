//
//  MainWeather.swift
//  weatherApp
//
//  Created by Eugene on 02.06.24.
//

import UIKit


extension MainVC: WeatherProtocol {
    
    func updateWeather(_ weatherManager: WeatherManager, weather: WeatherModel) {
        self.tempLabel.text = "\(weather.tempString)°C"
        self.precipitationIV.image = UIImage(systemName: weather.precipitation)
        cityLabel.text = weather.cityName
    }
    
    func weatherError(error: Error) {
        print("Weather error: \(error)")
    }
    
}
