//
//  MainVC.swift
//  weatherApp
//
//  Created by Eugene on 31.05.24.
//

import UIKit
import CoreLocation


class MainVC: UIViewController {
    
    let weatherManager = WeatherManager.shared
    let locationManager = CLLocationManager()

    typealias Degrees = Float
    var latitude: Degrees = 0.0
    var longitude: Degrees = 0.0
    
    let precipitationIV: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = .black
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    let tempLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = UIFont.systemFont(ofSize: 60, weight: .black)
        label.textAlignment = .center
        
        return label
    }()
    let cityLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = UIFont.systemFont(ofSize: 30, weight: .regular)
        label.textAlignment = .center

        return label
    }()
    
    let searchBarTF: UITextField = {
        let textField = UITextField()
        textField.autocorrectionType = .yes
        textField.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        textField.backgroundColor = .lightGray.withAlphaComponent(0.5)
        textField.textColor = .gray
        textField.tintColor = .gray
        textField.layer.cornerRadius = 16
        textField.placeholder = "Search"
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: textField.frame.height))
        textField.leftView = paddingView
        textField.leftViewMode = .always
        textField.rightView = paddingView
        textField.rightViewMode = .always
        
        return textField
    }()
    
    let currentLocationButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.tintColor = .black
        button.setImage(UIImage(systemName: "location.circle"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        return button
    }()
    let searchButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.tintColor = .black
        button.setImage(UIImage(systemName: "magnifyingglass"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        
        return button
    }()
    let mainCityButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.layer.cornerRadius = 16
        button.setTitle("Select city as main", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        weatherManager.weatherDelegate = self
        locationManager.delegate = self
        searchBarTF.delegate = self
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestLocation()
        
        currentLocationButton.addTarget(self, action: #selector(getCurrentLocation), for: .touchUpInside)
        searchButton.addTarget(self, action: #selector(startSearching), for: .touchUpInside)
        mainCityButton.addTarget(self, action: #selector(selectMainCity), for: .touchUpInside)
        
        mainSubviews()
        
        if let savedCity = UserDefaults.standard.string(forKey: "mainCity") {
            weatherManager.getWeather(for: savedCity)
        }
    }
    
}
