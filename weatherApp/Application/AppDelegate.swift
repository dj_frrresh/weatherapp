//
//  AppDelegate.swift
//  weatherApp
//
//  Created by Eugene on 31.05.24.
//

import UIKit


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        window?.rootViewController = MainVC()
        window?.makeKeyAndVisible()
        
        return true
    }

}

